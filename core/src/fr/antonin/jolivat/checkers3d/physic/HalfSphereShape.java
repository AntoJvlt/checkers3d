package fr.antonin.jolivat.checkers3d.physic;

import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.collision.BoundingBox;
import fr.antonin.jolivat.checkers3d.camera.Camera;

public class HalfSphereShape extends SphereShape
{
    public HalfSphereShape(BoundingBox bounds) {
        super(bounds);
    }

    @Override
    public boolean isVisible(Matrix4 transform, Camera cam) {
        return super.isVisible(transform, cam);
    }
}
