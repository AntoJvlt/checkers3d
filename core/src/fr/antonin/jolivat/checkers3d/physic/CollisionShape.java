package fr.antonin.jolivat.checkers3d.physic;

import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.BoundingBox;
import com.badlogic.gdx.math.collision.Ray;
import fr.antonin.jolivat.checkers3d.camera.Camera;

public abstract class CollisionShape
{
    protected final static Vector3 position = new Vector3();
    public final Vector3 center = new Vector3();
    public final Vector3 dimensions = new Vector3();

    public CollisionShape(BoundingBox bounds)
    {
        bounds.getCenter(center);
        bounds.getDimensions(dimensions);
    }

    public abstract boolean isVisible(Matrix4 transform, Camera cam);
    public abstract boolean rayIntersect(Matrix4 transform, Ray ray);
}
