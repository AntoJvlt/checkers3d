package fr.antonin.jolivat.checkers3d.physic;

import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.BoundingBox;
import com.badlogic.gdx.math.collision.Ray;
import fr.antonin.jolivat.checkers3d.camera.Camera;

public class SphereShape extends CollisionShape
{
    public float radius;

    public SphereShape(BoundingBox bounds)
    {
        super(bounds);
        radius = bounds.getDimensions(new Vector3()).len() / 2f;
    }

    @Override
    public boolean isVisible(Matrix4 transform, Camera cam)
    {
        return cam.frustum.sphereInFrustum(transform.getTranslation(position).add(center), radius);
    }

    @Override
    public boolean rayIntersect(Matrix4 transform, Ray ray)
    {
        transform.getTranslation(position).add(center);
        final float len = ray.direction.dot(position.x-ray.origin.x, position.y-ray.origin.y, position.z-ray.origin.z);
        float dist2 = position.dst2(ray.origin.x+ray.direction.x*len, ray.origin.y+ray.direction.y*len, ray.origin.z+ray.direction.z*len);
        return (dist2 <= radius * radius);
    }
}
