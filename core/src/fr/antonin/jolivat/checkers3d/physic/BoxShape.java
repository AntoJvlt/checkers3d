package fr.antonin.jolivat.checkers3d.physic;

import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.collision.BoundingBox;
import com.badlogic.gdx.math.collision.Ray;
import fr.antonin.jolivat.checkers3d.camera.Camera;

public class BoxShape extends CollisionShape
{
    public BoxShape(BoundingBox bounds) {
        super(bounds);
    }

    @Override
    public boolean isVisible(Matrix4 transform, Camera cam)
    {
        return cam.frustum.boundsInFrustum(transform.getTranslation(position).add(center), dimensions);
    }

    @Override
    public boolean rayIntersect(Matrix4 transform, Ray ray) {
        return false;
    }
}
