package fr.antonin.jolivat.checkers3d.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.*;
import fr.antonin.jolivat.checkers3d.core.Checkers3D;
import fr.antonin.jolivat.checkers3d.core.player.AIPlayer;
import fr.antonin.jolivat.checkers3d.core.player.Player;

public class MainMenu implements UI
{
    private Checkers3D checkers3D;
    private MainMenuController mainMenuController;
    private Skin skin;
    private Stage stage;
    private Color mainFontColor;
    private ShaderProgram fontShader;
    private Image player1Icon;
    private Image player2Icon;
    private ImageButton player1Arrow;
    private ImageButton player2Arrow;
    private ImageButton playButton;
    private Table ia1DifficultyTable;
    private Table ia2DifficultyTable;

    public MainMenu(Checkers3D checkers3D)
    {
        this.checkers3D = checkers3D;
        mainMenuController = new MainMenuController(this, checkers3D);
        stage = new Stage(new FillViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight()));
        skin = new Skin();
        TextureAtlas textureAtlas = new TextureAtlas(Gdx.files.internal("ui/ui.atlas"));
        skin.addRegions(textureAtlas);

        Table mainTable = new Table();
        mainTable.setFillParent(true);
        mainTable.top();
        mainTable.left();

        /* Table */
        Table table = new Table();
        table.center();
        table.top();

        table.setBackground(new TextureRegionDrawable(skin.getRegion("background")));
        table.getBackground().setMinWidth(0);
        table.getBackground().setMinHeight(0);

        ia1DifficultyTable = new Table();
        ia2DifficultyTable = new Table();

        mainFontColor = new Color(0.38f,0.32f, 0.25f,1.0f);

        fontShader = new ShaderProgram(Gdx.files.internal("fonts/font.vert"), Gdx.files.internal("fonts/font.frag"));
        if (!fontShader.isCompiled()) {
            Gdx.app.error("fontShader", "compilation failed:\n" + fontShader.getLog());
        }

        Texture texture = new Texture(Gdx.files.internal("fonts/impact.png"), true);
        texture.setFilter(Texture.TextureFilter.MipMapLinearLinear, Texture.TextureFilter.Linear);
        skin.add("impact", new BitmapFont(Gdx.files.internal("fonts/impact.fnt"), new TextureRegion(texture), false));
        texture = new Texture(Gdx.files.internal("fonts/aller_display.png"), true);
        texture.setFilter(Texture.TextureFilter.MipMapLinearLinear, Texture.TextureFilter.Linear);
        skin.add("aller_display", new BitmapFont(Gdx.files.internal("fonts/aller_display.fnt"), new TextureRegion(texture), false));

        player1Icon = new Image();
        player2Icon = new Image();
        player1Arrow = new ImageButton(new TextureRegionDrawable(skin.getRegion("player1arrow")));
        player2Arrow = new ImageButton(new TextureRegionDrawable(skin.getRegion("player2arrow")));
        playButton = new ImageButton(new TextureRegionDrawable(skin.getRegion("play")));

        playButton.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) { mainMenuController.startGame(); }
            @Override
            public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) { playButton.getImage().getColor().a = 0.5f; }
            @Override
            public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) { playButton.getImage().getColor().a = 1f; }
        });

        player1Arrow.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) { mainMenuController.execPlayerTypeSwitch(1); }
            @Override
            public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) { player1Arrow.getImage().getColor().a = 0.5f; }
            @Override
            public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) { player1Arrow.getImage().getColor().a = 1f; }
        });

        player2Arrow.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) { mainMenuController.execPlayerTypeSwitch(2); }
            @Override
            public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) { player2Arrow.getImage().getColor().a = 0.5f; }
            @Override
            public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) { player2Arrow.getImage().getColor().a = 1f; }
        });

        CustomShaderLabel vsLabel = new CustomShaderLabel("VS", new Label.LabelStyle(skin.getFont("impact"), mainFontColor));
        vsLabel.setFontScale(0.6f);
        vsLabel.setShader(fontShader);

        for(int i = 0; i < 32; i++)
        {
            table.add(new Actor()).size(5f, 5f);
        }

        table.row();
        table.add(ia1DifficultyTable).colspan(16).center().minHeight(35f);
        table.add(ia2DifficultyTable).colspan(16).center().minHeight(35f).row();
        table.add(player1Arrow).size(20f, 20f).colspan(4);
        table.add(new Actor()).colspan(1);
        table.add(player1Icon).size(35f, 35f).colspan(8);
        table.add(new Actor()).colspan(1);
        table.add(vsLabel).colspan(4);
        table.add(new Actor()).colspan(1);
        table.add(player2Icon).size(35f, 35f).colspan(8);
        table.add(new Actor()).colspan(1);
        table.add(player2Arrow).size(20f, 20f).colspan(4).row();
        table.add(playButton).size(100f, 35f).colspan(32).center();

        ia1DifficultyTable.padLeft(8f).padBottom(5f);
        ia2DifficultyTable.padRight(8f).padBottom(5f);
        table.padTop(10f);
        mainTable.padTop(155f);

        mainTable.add(table).width(250f).height(150f);
        stage.addActor(mainTable);

        checkers3D.getInputMultiplexer().addProcessor(stage);
        mainMenuController.init();
    }

    public void setPlayerIcon(int player, Class<? extends Player> type)
    {
        Image icon = player == 1 ? player1Icon : player2Icon;
        Table difficultyTable = player == 1 ? ia1DifficultyTable : ia2DifficultyTable;

        if(type == AIPlayer.class)
        {
            CustomShaderLabel difficultyLabel = new CustomShaderLabel("Difficulté", new Label.LabelStyle(skin.getFont("aller_display"), mainFontColor));
            difficultyLabel.setFontScale(0.3f);
            difficultyLabel.setShader(fontShader);

            ImageButton[] buttons = new ImageButton[3];
            buttons[0] = new ImageButton(skin.getDrawable("one"));
            buttons[1] = new ImageButton(skin.getDrawable("two"));
            buttons[2] = new ImageButton(skin.getDrawable("three"));

            for(int i = 0; i < 3; i++)
            {
                ImageButton imageButton = buttons[i];
                int level = i + 1;
                imageButton.addListener(new ClickListener(){
                    @Override
                    public void clicked(InputEvent event, float x, float y) { mainMenuController.execAILevelChange(player, level); }
                    @Override
                    public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) { imageButton.getImage().getColor().a = 0.5f; }
                    @Override
                    public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) { imageButton.getImage().getColor().a = 1f; }
                });
            }

            difficultyTable.add(difficultyLabel).colspan(3).row();
            difficultyTable.add(buttons[0]).size(13f, 13f).padTop(2f);
            difficultyTable.add(buttons[1]).size(13f, 13f).padTop(2f);
            difficultyTable.add(buttons[2]).size(13f, 13f).padTop(2f);

            icon.setDrawable(skin.getDrawable("ai"));
        }
        else
        {
            difficultyTable.clearChildren();
            icon.setDrawable(skin.getDrawable("human"));
        }
    }

    public void setAIIconSelected(int player, int previousLevel, int currentLevel)
    {
        Table difficultyTable = player == 1 ? ia1DifficultyTable : ia2DifficultyTable;

        if(previousLevel == 1)
            ((ImageButton)difficultyTable.getCells().get(1).getActor()).getStyle().imageUp = skin.getDrawable("one");
        else if(previousLevel == 2)
            ((ImageButton)difficultyTable.getCells().get(2).getActor()).getStyle().imageUp = skin.getDrawable("two");
        else if(previousLevel == 3)
            ((ImageButton)difficultyTable.getCells().get(3).getActor()).getStyle().imageUp = skin.getDrawable("three");

        if(currentLevel == 1)
            ((ImageButton)difficultyTable.getCells().get(1).getActor()).getStyle().imageUp = skin.getDrawable("oneSelected");
        else if(currentLevel == 2)
            ((ImageButton)difficultyTable.getCells().get(2).getActor()).getStyle().imageUp = skin.getDrawable("twoSelected");
        else if(currentLevel == 3)
            ((ImageButton)difficultyTable.getCells().get(3).getActor()).getStyle().imageUp = skin.getDrawable("threeSelected");
    }

    @Override
    public void render()
    {
        stage.getViewport().apply();
        stage.act(Gdx.graphics.getDeltaTime());
        stage.draw();
    }

    public void close()
    {
        checkers3D.setUi(null);
        checkers3D.getInputMultiplexer().removeProcessor(stage);
        dispose();
    }

    @Override
    public void dispose()
    {
        skin.dispose();
        fontShader.dispose();
        stage.dispose();
    }

    @Override
    public void update(int width, int height) {
        stage.getViewport().update(width, height);
    }

    @Override
    public boolean isBlocking() {
        return true;
    }
}
