package fr.antonin.jolivat.checkers3d.ui;

public interface UI
{
    void render();
    void dispose();
    void update(int width, int height);
    boolean isBlocking();
}
