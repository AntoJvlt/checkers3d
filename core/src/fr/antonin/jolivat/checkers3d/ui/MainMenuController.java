package fr.antonin.jolivat.checkers3d.ui;

import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;
import fr.antonin.jolivat.checkers3d.camera.ActionTravellingDone;
import fr.antonin.jolivat.checkers3d.camera.StraightTravelling;
import fr.antonin.jolivat.checkers3d.core.Checkers3D;
import fr.antonin.jolivat.checkers3d.core.GameParameter;
import fr.antonin.jolivat.checkers3d.core.player.AIPlayer;
import fr.antonin.jolivat.checkers3d.core.player.HumanPlayer;
import fr.antonin.jolivat.checkers3d.core.player.Player;

public class MainMenuController
{
    private MainMenu mainMenu;
    private Checkers3D checkers3D;
    private GameParameter gameParameter;

    public MainMenuController(MainMenu mainMenu, Checkers3D checkers3D)
    {
        this.mainMenu = mainMenu;
        this.checkers3D = checkers3D;
        gameParameter = new GameParameter();
    }

    public void init()
    {
        gameParameter.setPlayer1(new HumanPlayer());
        gameParameter.setPlayer2(new AIPlayer());
        mainMenu.setPlayerIcon(1, HumanPlayer.class);
        mainMenu.setPlayerIcon(2, AIPlayer.class);
    }

    public void startGame()
    {
        mainMenu.close();
        checkers3D.getCamera().getCameraTravellerQueue().remove();
        checkers3D.getCamera().addTraveller(new StraightTravelling(checkers3D.getCamera(), false, new Vector3(-20, 13, -20), new Vector3(0, 0, 0), 5f, null));
        checkers3D.getCamera().addTraveller(new StraightTravelling(checkers3D.getCamera(), false, new Vector3(20, 13, 0), new Vector3(0, 0, 0), 0.5f, () -> checkers3D.startGame()));
    }

    public void execPlayerTypeSwitch(int player)
    {
        Player p = getPlayerFromInt(player);

        if(p instanceof HumanPlayer)
        {
            AIPlayer aiPlayer = new AIPlayer();

            if(player == 1)
                gameParameter.setPlayer1(aiPlayer);
            else
                gameParameter.setPlayer2(aiPlayer);

            mainMenu.setPlayerIcon(player, AIPlayer.class);

        }else if(p instanceof AIPlayer)
        {
            HumanPlayer humanPlayer = new HumanPlayer();

            if(player == 1)
                gameParameter.setPlayer1(humanPlayer);
            else
                gameParameter.setPlayer2(humanPlayer);

            mainMenu.setPlayerIcon(player, HumanPlayer.class);
        }
    }

    public void execAILevelChange(int player, int level)
    {
        Player p = getPlayerFromInt(player);

        if(p instanceof AIPlayer)
        {
            AIPlayer ai = (AIPlayer)p;

            if(ai.getDifficulty() != level)
            {
                mainMenu.setAIIconSelected(player, ai.getDifficulty(), level);
                ai.setDifficulty(level);
            }
        }
    }

    private Player getPlayerFromInt(int player)
    {
        return player == 1 ? gameParameter.getPlayer1() : gameParameter.getPlayer2();
    }
}
