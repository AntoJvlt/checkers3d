package fr.antonin.jolivat.checkers3d.ui;

import com.badlogic.gdx.Gdx;

public class ResponsiveUI
{
    public static void rebuildUI(UI ui)
    {
        float scale;

        int width = Gdx.graphics.getWidth();
        int height = Gdx.graphics.getHeight();

        if(width > 640 && height > 480 && width <= 1400 && height <= 800)
            scale = 0.9f;
        else if(width >= 1400 && height >= 800 && width <= 1920 && height <= 1080)
            scale = 0.8f;
        else if(width > 1920 && height > 1080)
            scale = 1.6f;
        else
            scale = 0.9f;
    }
}
