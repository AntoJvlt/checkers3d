package fr.antonin.jolivat.checkers3d.core;

import com.badlogic.gdx.math.Vector2;
import fr.antonin.jolivat.checkers3d.entity.Pawn;
import fr.antonin.jolivat.checkers3d.entity.Square;

import java.util.ArrayList;

public class Board
{
    public final static int WIDTH = 8;
    public final static int HEIGHT = 8;

    private ArrayList<Pawn> pawns = new ArrayList<>();
    private ArrayList<Square> squares = new ArrayList<>();

    public boolean isInBounds(Vector2 xy)
    {
        return xy.x >= 0 && xy.x < WIDTH && xy.y >= 0 && xy.y < HEIGHT;
    }

    /*public Pawn getPawnAt(Vector2 xy)
    {
        return pawns.stream().filter(p -> p.getX() == xy.x && p.getY() == xy.y).collect(Collectors.toList()).get(0);
    }

    public Square getSquareAt(Vector2 xy)
    {
        return squares.stream().filter(s -> s.getX() == xy.x && s.getY() == xy.y).collect(Collectors.toList()).get(0);
    }*/

    public ArrayList<Pawn> getPawns() {
        return pawns;
    }

    public ArrayList<Square> getSquares() {
        return squares;
    }
}
