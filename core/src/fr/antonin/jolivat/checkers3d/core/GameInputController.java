package fr.antonin.jolivat.checkers3d.core;

import com.badlogic.gdx.InputAdapter;
import fr.antonin.jolivat.checkers3d.entity.Pawn;
import fr.antonin.jolivat.checkers3d.entity.Square;

import java.util.stream.Collectors;

public class GameInputController extends InputAdapter
{
    private Pawn pickedPawn = null;
    private Checkers3D checkers3D;

    public GameInputController(Checkers3D checkers3D)
    {
        this.checkers3D = checkers3D;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button)
    {
        Pawn pawn = null;
        Square square = null;

        for(Pawn p : checkers3D.getMap().getBoard().getPawns().stream().filter(p -> p.isVisible(checkers3D.getCamera())).collect(Collectors.toList()))
            if(p.rayIntersect(checkers3D.getCamera().getPickRay(screenX, screenY)))
                pawn = p;

        for(Square s : checkers3D.getMap().getBoard().getSquares().stream().filter(s -> s.isVisible(checkers3D.getCamera())).collect(Collectors.toList()))
            if(s.rayIntersect(checkers3D.getCamera().getPickRay(screenX, screenY)))
                square = s;

        System.out.println(checkers3D.getMap().getBoard().getSquares().stream().filter(s -> s.isVisible(checkers3D.getCamera())).count());
        return false;
    }
}
