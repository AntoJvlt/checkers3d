package fr.antonin.jolivat.checkers3d.core;

import fr.antonin.jolivat.checkers3d.core.player.Player;

public class GameParameter
{
    Player player1;
    Player player2;

    public GameParameter()
    {

    }

    public void setPlayer1(Player player1) {
        this.player1 = player1;
    }

    public void setPlayer2(Player player2) {
        this.player2 = player2;
    }

    public Player getPlayer1() {
        return player1;
    }

    public Player getPlayer2() {
        return player2;
    }
}
