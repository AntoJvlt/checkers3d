package fr.antonin.jolivat.checkers3d.core;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import fr.antonin.jolivat.checkers3d.entity.Entity;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class Renderer
{
    private final ModelBatch modelBatch = new ModelBatch();
    private Checkers3D checkers3D;
    private Environment environment;

    public Renderer(Checkers3D checkers3D)
    {
        this.checkers3D = checkers3D;

        environment = new Environment();
        environment.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.4f, 0.4f, 0.4f, 1f));
        environment.add(new DirectionalLight().set(0.8f, 0.8f, 0.8f, -1f, -0.8f, -0.2f));
    }

    public void render()
    {
        Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT | (Gdx.graphics.getBufferFormat().coverageSampling?GL20.GL_COVERAGE_BUFFER_BIT_NV:0));

        if(!checkers3D.getMap().isLoading())
        {
            modelBatch.begin(checkers3D.getCamera());
            modelBatch.render(checkers3D.getMap().getDecorativeEntities().stream().filter(e -> e.isVisible(checkers3D.getCamera())).collect(Collectors.toList()), environment);
            modelBatch.render(checkers3D.getMap().getBoard().getPawns().stream().filter(e -> e.isVisible(checkers3D.getCamera())).collect(Collectors.toList()), environment);
            modelBatch.end();
        }

        if(checkers3D.getUi() != null) checkers3D.getUi().render();
    }
}
