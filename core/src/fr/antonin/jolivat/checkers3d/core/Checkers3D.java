package fr.antonin.jolivat.checkers3d.core;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.math.Vector3;
import fr.antonin.jolivat.checkers3d.camera.Camera;
import fr.antonin.jolivat.checkers3d.camera.CircleTravelling;
import fr.antonin.jolivat.checkers3d.maps.Map;
import fr.antonin.jolivat.checkers3d.ui.MainMenu;
import fr.antonin.jolivat.checkers3d.ui.UI;

public class Checkers3D implements ApplicationListener
{
	private final AssetManager assetManager = new AssetManager();
	private final InputMultiplexer inputMultiplexer = new InputMultiplexer();
	private Renderer renderer;
	private UI ui;
	private Camera camera;
	private Map map;
	private Game game;
	private GameInputController gameInputController;

	@Override
	public void create ()
	{
	    renderer = new Renderer(this);
		camera = new Camera(this);
		map = new Map(this);
		game = new Game(this);
		gameInputController = new GameInputController(this);

		camera.addTraveller(new CircleTravelling(camera, true, new Vector3(0, 0, 0), 1500, null));
		ui = new MainMenu(this);
		Gdx.input.setInputProcessor(inputMultiplexer);
	}

	public void startGame()
	{
		inputMultiplexer.addProcessor(gameInputController);
		camera.setManualControl(true);
	}

	@Override
	public void resize(int width, int height)
	{
		camera.viewportWidth = width;
		camera.viewportHeight = height;

		if(ui != null)
			ui.update(width, height);
	}

	@Override
	public void render ()
	{
		if(!map.isLoading() && assetManager.isFinished())
		{
			renderer.render();
			camera.updateAll();
		}else if(!assetManager.isFinished())
			assetManager.update();
		else if(map.isLoading())
			map.tryLoad();
	}

	@Override
	public void pause()
	{

	}

	@Override
	public void resume()
	{

	}

	@Override
	public void dispose ()
	{
		if(ui != null) ui.dispose();
		assetManager.dispose();
	}

	public void setUi(UI uiModel)
	{
		ui = uiModel;
	}

	public AssetManager getAssetManager() {
		return assetManager;
	}

	public InputMultiplexer getInputMultiplexer() {
		return inputMultiplexer;
	}

    public UI getUi() {
        return ui;
    }

    public Camera getCamera() {
		return camera;
	}

	public Map getMap() {
		return map;
	}

	public Game getGame() {
		return game;
	}
}
