package fr.antonin.jolivat.checkers3d.core.player;

public class AIPlayer extends Player
{
    private int difficulty;

    public void setDifficulty(int difficulty) {
        this.difficulty = difficulty;
    }

    public int getDifficulty() {
        return difficulty;
    }
}
