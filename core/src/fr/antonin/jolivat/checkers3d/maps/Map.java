package fr.antonin.jolivat.checkers3d.maps;

import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.BoundingBox;
import fr.antonin.jolivat.checkers3d.core.Board;
import fr.antonin.jolivat.checkers3d.core.Checkers3D;
import fr.antonin.jolivat.checkers3d.entity.DecorativeEntity;
import fr.antonin.jolivat.checkers3d.entity.DynamicEntity;
import fr.antonin.jolivat.checkers3d.entity.Pawn;
import fr.antonin.jolivat.checkers3d.entity.Square;
import fr.antonin.jolivat.checkers3d.physic.BoxShape;
import fr.antonin.jolivat.checkers3d.physic.HalfSphereShape;
import fr.antonin.jolivat.checkers3d.physic.SphereShape;

import java.util.ArrayList;

public class Map
{
    private static final String SCENE_FILE = "scene/basic_map.g3dj";
    private Checkers3D checkers3D;
    private Board board;
    private boolean loading;
    private ArrayList<DecorativeEntity> decorativeEntities = new ArrayList<>();

    public Map(Checkers3D checkers3D)
    {
        this.checkers3D = checkers3D;
        board = new Board();
        checkers3D.getAssetManager().load(SCENE_FILE, Model.class);
        loading = true;
    }

    public void offSetAll(Vector3 vector3)
    {
        for (DecorativeEntity i : decorativeEntities)
            i.transform.setToTranslation(vector3);

        for (Pawn p : board.getPawns())
            p.transform.setToTranslation(vector3);
    }

    public void tryLoad()
    {
        if(checkers3D.getAssetManager().isLoaded(SCENE_FILE, Model.class))
        {
            Model model = checkers3D.getAssetManager().get(SCENE_FILE, Model.class);
            BoundingBox bounds = new BoundingBox();

            for(int i = 0; i < model.nodes.size; i++)
            {
                String name = model.nodes.get(i).id;

                switch(name.split("-")[0])
                {
                    case "pawn":
                        model.nodes.get(i).calculateBoundingBox(bounds);
                        board.getPawns().add(new Pawn(model, name, new HalfSphereShape(bounds)));
                        break;
                    case "square":
                        model.nodes.get(i).calculateBoundingBox(bounds);
                        int x = Integer.parseInt(name.split("-")[1]);
                        int y = Integer.parseInt(name.split("-")[2]);
                        board.getSquares().add(new Square(model, name, new BoxShape(bounds)));
                        break;
                    default:
                        decorativeEntities.add(new DecorativeEntity(model, name));
                        break;
                }
            }
            loading = false;
        }
    }

    public boolean isLoading()
    {
        return loading;
    }

    public Board getBoard() {
        return board;
    }

    public ArrayList<DecorativeEntity> getDecorativeEntities() {
        return decorativeEntities;
    }
}
