package fr.antonin.jolivat.checkers3d.camera;

import com.badlogic.gdx.math.Vector3;

public class StraightTravelling extends CameraTraveller
{
    private Vector3 destination;
    private Vector3 direction;
    private Vector3 lookAt;
    private float speed;
    private double lastDist;
    private boolean isInit = false;
    private boolean isArrived = false;

    public StraightTravelling(Camera camera, boolean looping, Vector3 destination, Vector3 lookAt, float speed, ActionTravellingDone actionTravellingDone) {
        super(camera, looping, actionTravellingDone);
        this.destination = destination;
        this.lookAt = lookAt;
        this.speed = speed;
    }

    @Override
    public void execUpdate()
    {
        if(isInit)
        {
            verifyArrived();

            camera.position.add(direction.cpy().scl(speed));
            camera.lookAt(lookAt);
            camera.up.set(Vector3.Y);
        }else
            init();
    }

    public void init()
    {
        direction = destination.cpy().sub(camera.position).nor();
        lastDist = camera.position.dst(destination);
        isInit = true;
    }

    @Override
    public boolean hasToStop()
    {
        if(isArrived)
        {
            camera.position.set(destination);
            camera.lookAt(lookAt);
            camera.up.set(Vector3.Y);
            return true;
        }else
            return false;
    }

    public void verifyArrived()
    {
        double dist = camera.position.dst(destination);
        if(dist > lastDist)
            isArrived = true;
        else
            lastDist = dist;
    }
}
