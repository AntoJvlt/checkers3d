package fr.antonin.jolivat.checkers3d.camera;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.g3d.utils.CameraInputController;
import fr.antonin.jolivat.checkers3d.core.Checkers3D;

import java.util.PriorityQueue;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class Camera extends PerspectiveCamera
{
    private final static int FOV = 67;
    private Checkers3D checkers3D;
    private Queue<CameraTraveller> cameraTravellerQueue = new ConcurrentLinkedQueue<>();
    private CameraInputController cameraInputController;
    private CameraController cameraController;

    public Camera(Checkers3D checkers3D)
    {
        super(FOV, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        this.checkers3D = checkers3D;
        cameraInputController = new CameraInputController(this);
        cameraController = new CameraController(this);

        position.set(20f, 13f, -10);
        lookAt(0,0,0);
        near = 1f;
        far = 3000f;
        update();
    }

    public void setManualControl(boolean on)
    {
        if(on)
        {
            checkers3D.getInputMultiplexer().addProcessor(cameraController);
            checkers3D.getInputMultiplexer().addProcessor(cameraInputController);
        }
        else
        {
            checkers3D.getInputMultiplexer().removeProcessor(cameraController);
            checkers3D.getInputMultiplexer().removeProcessor(cameraInputController);
        }
    }

    public void updateAll()
    {
        if(!cameraTravellerQueue.isEmpty())
            cameraTravellerQueue.element().update();

        super.update();
    }

    public void addTraveller(CameraTraveller cameraTraveller)
    {
        cameraTravellerQueue.add(cameraTraveller);
    }

    public Queue<CameraTraveller> getCameraTravellerQueue() {
        return cameraTravellerQueue;
    }
}
