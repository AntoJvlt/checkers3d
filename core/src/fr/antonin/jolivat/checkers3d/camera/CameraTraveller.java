package fr.antonin.jolivat.checkers3d.camera;

import com.badlogic.gdx.math.Vector3;

public abstract class CameraTraveller
{
    protected ActionTravellingDone actionTravellingDone;
    protected Camera camera;
    protected boolean looping;

    public CameraTraveller(Camera camera, boolean looping, ActionTravellingDone actionTravellingDone)
    {
        this.camera = camera;
        this.looping = looping;
        this.actionTravellingDone = actionTravellingDone;
    }

    public void stop()
    {
        if(actionTravellingDone != null)
            actionTravellingDone.action();

        camera.getCameraTravellerQueue().remove();
    }

    public void update()
    {
        if(!looping && hasToStop())
            stop();
        else
            execUpdate();
    }

    public abstract void execUpdate();

    public abstract boolean hasToStop();
}
