package fr.antonin.jolivat.checkers3d.camera;

import com.badlogic.gdx.InputProcessor;

public class CameraController implements InputProcessor
{
    private Camera camera;
    private int lastScreenX;
    private int lastScreenY;
    private float lastCamX;

    public CameraController(Camera camera)
    {
        this.camera = camera;
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        lastScreenX = screenX;
        lastScreenY = screenY;
        lastCamX = camera.position.x;
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer)
    {
        /*if((camera.position.y <= 5 && screenY < lastScreenY) || ((lastCamX - camera.position.x) > lastCamX && screenY > lastScreenY))
        {
            lastScreenX = screenX;
            lastScreenY = screenY;
            return true;
        }

        lastScreenX = screenX;
        lastScreenY = screenY;
        lastCamX = camera.position.x;*/
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
