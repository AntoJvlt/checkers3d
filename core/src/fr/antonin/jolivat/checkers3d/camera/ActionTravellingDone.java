package fr.antonin.jolivat.checkers3d.camera;

public interface ActionTravellingDone
{
    void action();
}
