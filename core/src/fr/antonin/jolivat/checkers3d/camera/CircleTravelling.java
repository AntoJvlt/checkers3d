package fr.antonin.jolivat.checkers3d.camera;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector3;

public class CircleTravelling extends CameraTraveller
{
    private float radius;
    private float time;
    private float angle;
    private float previousAngle;
    private int period;

    public CircleTravelling(Camera camera, boolean looping, Vector3 circleCenter, int period, ActionTravellingDone actionTravellingDone)
    {
        super(camera, looping, actionTravellingDone);
        this.period = period;
        radius = (float)Math.sqrt(Math.pow(camera.position.x - circleCenter.x, 2) + Math.pow(camera.position.z - circleCenter.z, 2));
        time = 0;
        angle = 0;
        previousAngle = 0;
    }

    @Override
    public void execUpdate()
    {
        if(previousAngle >= 360)
            time = 0;

        float x = (float)(radius * Math.cos(((2 * Math.PI) / period) * time));
        float z = (float)(radius * Math.sin(((2 * Math.PI) / period) * time));

        angle = (float)Math.toDegrees(((2 * Math.PI) / period) * time);
        camera.position.set(x, camera.position.y, z);
        camera.rotate((angle - previousAngle) * -1, 0, 1, 0);

        previousAngle = angle;
        time++;
    }

    @Override
    public boolean hasToStop() {
        return angle >= 360;
    }
}
