package fr.antonin.jolivat.checkers3d.entity;

import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.math.collision.BoundingBox;
import fr.antonin.jolivat.checkers3d.camera.Camera;
import fr.antonin.jolivat.checkers3d.physic.CollisionShape;
import fr.antonin.jolivat.checkers3d.physic.SphereShape;

public abstract class Entity extends ModelInstance
{
    private CollisionShape shapeForCulling;

    public Entity(Model model, String name, CollisionShape shapeForCulling)
    {
        super(model, name);
        this.shapeForCulling = shapeForCulling;
    }

    public boolean isVisible(Camera cam)
    {
        return shapeForCulling.isVisible(transform, cam);
    }
}
