package fr.antonin.jolivat.checkers3d.entity;

import com.badlogic.gdx.graphics.g3d.Model;
import fr.antonin.jolivat.checkers3d.physic.CollisionShape;

public class Pawn extends DynamicEntity
{
    public Pawn(Model model, String name, CollisionShape shapeForCulling, CollisionShape shapeForPhysic) {
        super(model, name, shapeForCulling, shapeForPhysic);
    }
}
