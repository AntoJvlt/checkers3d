package fr.antonin.jolivat.checkers3d.entity;

import com.badlogic.gdx.graphics.g3d.Model;
import fr.antonin.jolivat.checkers3d.physic.CollisionShape;

public class DecorativeEntity extends Entity
{
    public DecorativeEntity(Model model, CollisionShape shapeForCulling, String name) {
        super(model, name, shapeForCulling);
    }
}
