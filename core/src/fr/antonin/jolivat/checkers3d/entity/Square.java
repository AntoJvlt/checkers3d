package fr.antonin.jolivat.checkers3d.entity;

import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import fr.antonin.jolivat.checkers3d.physic.CollisionShape;

public class Square extends DynamicEntity
{
    public Square(Model model, String name, CollisionShape shapeForCulling, CollisionShape shapeForPhysic) {
        super(model, name, shapeForCulling, shapeForPhysic);
    }
}
