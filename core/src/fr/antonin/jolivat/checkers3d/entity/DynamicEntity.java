package fr.antonin.jolivat.checkers3d.entity;

import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.collision.Ray;
import fr.antonin.jolivat.checkers3d.physic.CollisionShape;

public abstract class DynamicEntity extends Entity
{
    private int x;
    private int y;
    private CollisionShape shapeForPhysic;

    public DynamicEntity(Model model, String name, CollisionShape shapeForCulling, CollisionShape shapeForPhysic) {
        super(model, name, shapeForCulling);

        if(shapeForPhysic != null)
            this.shapeForPhysic = shapeForPhysic;
        else
            this.shapeForPhysic = shapeForCulling;
    }

    public boolean rayIntersect(Ray ray)
    {
        return shapeForPhysic.rayIntersect(transform, ray);
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
